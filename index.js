const MONTH_NAMES = [
	'Janeiro',
	'Fevereiro',
	'Março',
	'Abril',
	'Maior',
	'Junho',
	'Julho',
	'Agosto',
	'Setembro',
	'Outubro',
	'Novembro',
	'Dezembro',
];

function convertCurrencyToNumber(value) {
	return parseInt(
		value.replace(/R\$ +/gi, '').replace('.', '').replace(',', '')
	);
}

function convertNumberToCurrency(value) {
	return Intl.NumberFormat('pt-BR', {
		style: 'currency',
		currency: 'BRL',
	}).format(value / 100);
}

function consolidatedInvoices(tableId) {
	let tableRows = Array.from(document.querySelectorAll(`#${tableId} tbody tr`));
	let consolidated = {};

	tableRows.forEach((row) => {
		let month = row.children[2].textContent;
		let valueInNumber = convertCurrencyToNumber(row.children[1].textContent);

		consolidated[month]
			? (consolidated[month] = consolidated[month] + valueInNumber)
			: (consolidated[month] = valueInNumber);
	});
	return consolidated;
}

function createTable() {
	let table = document.createElement('table');
	table.className = 'table';
	table.createTHead();
	table.createTBody();
	return table;
}

function createTableRow(tag, ...values) {
	let row = document.createElement('tr');

	values.forEach((value) => {
		let element = document.createElement(tag);
		element.innerHTML = value;
		row.appendChild(element);
	});
	return row;
}

function createConsolidatedTable(consolidated) {
	let table = createTable();
	let months = Object.keys(consolidated).sort();

	table.tHead.appendChild(createTableRow('th', 'Mês', 'Total gasto'));

	months.forEach((month) => {
		let fullMonth = MONTH_NAMES[parseInt(month) - 1];
		let totalForMonth = consolidated[month];
		table.tBodies[0].appendChild(
			createTableRow('td', fullMonth, convertNumberToCurrency(totalForMonth))
		);
	});
	return table;
}

let body = document.getElementsByTagName('body')[0];
let h2 = document.createElement('h2');

h2.innerHTML = 'Valores consolidados';
h2.className = 'consolidated-report';
h2.style.textAlign = 'center';

body.appendChild(document.createElement('br'));
body.appendChild(h2);
body.appendChild(document.createElement('br'));
body.appendChild(createConsolidatedTable(consolidatedInvoices('tb_fatura')));
